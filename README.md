Used with [Terraform](https://www.terraform.io/downloads.html "Download Terraform for any platform here, it's free and open source"), these files will install an __ubuntu 18.04 LTS__ on an __EC2 AWS instance__, make said instance reachable through __SSH on port 22__, and install an __interactive ansible tutorial__ (in French) and its prerequisites. _The source for the Ansible interactive tutorial is [here](https://github.com/goffinet/ansible-interactive-tutorial), and is taken from [this course](https://iac.goffinet.org/ansible-fondamental/) on Ansible fundamentals._

Note that references to __AWS and SSH keys__ have been saved to a file called __secrets.auto.tfvars__, which you will need to create and place __in the same folder__. It is not part of this repository. Guess why.

Once Terraform has successfully created your instance, you will need to __connect to it through SSH and enter the following commands__ to start the tutorial:

```
cd ansible-interactive-tutorial
bash tutorial.sh
````

The __secrets.auto.tfvars__ looks like this:

```
aws_access_key = "my_aws_access_key"

aws_secret_key = "my_aws_secret_key"

key_name = "my_key_name"

private_key_path = "local_path_to_private_key.pem"
```
