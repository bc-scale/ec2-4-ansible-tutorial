#This uses the default VPC.  It WILL NOT delete it on destroy.
resource "aws_default_vpc" "default" {

}

resource "aws_security_group" "allow_ssh" {
  name        = "EC2 for Ansible Tutorial"
  description = "Allow ports for Ansible Tutorial"
  vpc_id      = aws_default_vpc.default.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "ansible" {
  # Ubuntu Server 18.04 LTS (HVM), SSD Volume Type in eu-central-1
  ami                    = "ami-0cc0a36f626a4fdf5"
  instance_type          = "t2.micro"
  key_name               = var.key_name
  vpc_security_group_ids = [aws_security_group.allow_ssh.id]
  tags                   = { Name = "EC2 instance for Ansible Tutorial" }

  connection {
    type        = "ssh"
    host        = self.public_ip
    user        = "ubuntu"
    private_key = file(var.private_key_path)

  }

  provisioner "remote-exec" {
    inline = [
# Clone the tutorial from repository
      "git clone https://github.com/goffinet/ansible-interactive-tutorial.git",
# install docker and put ubuntu user in docker group
      "sudo curl -fsSL https://get.docker.com -o get-docker.sh && sh get-docker.sh && sudo usermod -aG docker ubuntu",
# after this, login with ssh and run:
#      "cd ansible-interactive-tutorial",
#      "bash tutorial.sh"
    ]
  }
}